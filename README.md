# U-COSFIRE
Repository with code and examples from the paper 

[[1] Ramachandran, S., Strisciuglio, N., Vinekar, A. et al. U-COSFIRE filters for vessel tortuosity quantification with application to automated diagnosis of retinopathy of prematurity. Neural Comput & Applic (2020). https://doi.org/10.1007/s00521-019-04697-6](https://link.springer.com/article/10.1007/s00521-019-04697-6)

## How to start
Run the script _BeforeUsing.m_ to compile the necessary files, before running the main example code (Note: you need a MEX compiler).

Run the script _main.m_ for example application of the B-COSFIRE and U-COSFIRE filters proposed in [1].

## Parameters
The example images are taken from the STARE data set. The parameters used for the processing are those used in [1].
For better performance, one should adapt the parameter values to the specific data set.


## Reference publications
If you use the code of U-COSFIRE filters for your application, please cite the following articles. 

__Original paper:__  

	@article{ucosfire2020,
	title = "{U-COSFIRE} filters for vessel tortuosity quantification with application to automated diagnosis of retinopathy of prematurity",
	journal = "Neural Computing and Applications ",
	volume = "",
	number = "",
	pages = "",
	year = "2020",
	note = "",
	issn = "1433-3058",
	doi = "https://doi.org/10.1007/s00521-019-04697-6",
	author = "Sivakumar Ramachandran and Nicola Strisciuglio and Anand Vinekar and Renu John and George Azzopardi ",
	} 