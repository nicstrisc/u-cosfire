function [resp oriensmap] = BCOSFIRE(image, filter1, filter2)
% 
%% Model configuration
% Prototype pattern
x = 101; y = 101; % center
line1(:, :) = zeros(201);
line1(x, :) = 1; %prototype line

% Symmetric filter params
symmfilter = cell(1);
symm_params = SystemConfig;
% COSFIRE params
symm_params.inputfilter.DoG.sigmalist = filter1.sigma;
symm_params.COSFIRE.rholist = 0:2:filter1.len;
symm_params.COSFIRE.sigma0 = filter1.sigma0 / 6;
symm_params.COSFIRE.alpha = filter1.alpha / 6;
% Orientations
numoriens = 12;
symm_params.invariance.rotation.psilist = 0:pi/numoriens:pi-pi/numoriens;
% Configuration
symmfilter{1} = configureCOSFIRE(line1, round([y x]), symm_params);
% viewCOSFIREstructure(symmfilter{1})
% Show the structure of the COSFIRE filter
% showCOSFIREstructure(symmfilter);

% % Asymmetric filter params
asymmfilter = cell(1);
asymm_params = SystemConfig;
% COSFIRE params
asymm_params.inputfilter.DoG.sigmalist = filter2.sigma;
asymm_params.COSFIRE.rholist = 0:2:filter2.len;
asymm_params.COSFIRE.sigma0 = filter2.sigma0 / 6;
asymm_params.COSFIRE.alpha = filter2.alpha / 6;
% Orientations
numoriens = 48;
asymm_params.invariance.rotation.psilist = 0:2*pi/numoriens:(2*pi)-(2*pi/numoriens);
% Configuration
asymmfilter{1} = configureCOSFIRE(line1, round([y x]), asymm_params);
asymmfilter{1}.tuples(:, asymmfilter{1}.tuples(4,:) > pi) = []; % Deletion of on side of the filter
% viewCOSFIREstructure(asymmfilter{1})
% % Show the structure of the COSFIRE operator
% % showCOSFIREstructure(asymmfilter);
% 
image = 1 - image;

% Apply the symmetric B-COSFIRE to the input image
rot1 = applyCOSFIRE(image, symmfilter);
rot2 = applyCOSFIRE(image, asymmfilter);

if nargout == 1
%    The code as presented in the paper
   rot1 = max(rot1{1},[],3);
   rot2 = max(rot2{1},[],3);
   resp = rot1 + rot2;
elseif nargout == 2   
%     Modified code to also give the orientation map as output
    for i = 1:size(rot1{1},3)
        resp(:,:,i) = rot1{1}(:,:,i) + max(rot2{1}(:,:,i),rot2{1}(:,:,i+12));    
    end
    [resp,oriensmap] = max(resp,[],3);
    oriensmap = symm_params.invariance.rotation.psilist(oriensmap);
end