% function output = Application( )
% Delineation of blood vessels in retinal images based on combination of BCOSFIRE filters responses.
%
% VERSION 09/09/2014
% CREATED BY: George Azzopardi (1), Nicola Strisciuglio (1,2), Mario Vento (2) and Nicolai Petkov (1)
%             1) University of Groningen, Johann Bernoulli Institute for Mathematics and Computer Science, Intelligent Systems
%             1) University of Salerno, Dept. of Information Eng., Electrical Eng. and Applied Math., MIVIA Lab
%
%   If you use this script please cite the following paper:
%   "George Azzopardi, Nicola Strisciuglio, Mario Vento, Nicolai Petkov, 
%   Trainable COSFIRE filters for vessel delineation with application to retinal images, 
%   Medical Image Analysis, Available online 3 September 2014, ISSN 1361-8415, 
%   http://dx.doi.org/10.1016/j.media.2014.08.002"
%
clear all
close all
% EXAMPLE APPLICATION.
Ax=1600;Ay=1200;
% Example with an image from DRIVE data set
image1 = imread('f2.jpg');
image1=rgb2ycbcr(image1);
image1=image1(:,:,1);
% image1 = double(imresize(image1,[600 NaN],'bicubic'));
image1 = image1 ./ 255;
sf = size(image1,1) / 584; % 584 is the height of the images in DRIVE data set
%% Symmetric filter params
symmfilter1 = struct();
symmfilter1.sigma     = 2.4 * sf;
symmfilter1.len       = 8 * sf;
symmfilter1.sigma0    = 0.2 * sf;
symmfilter1.alpha     = 0.1 * sf;

%% Asymmetric filter params
asymmfilter1 = struct();
asymmfilter1.sigma     = 1.8 * sf;
asymmfilter1.len       = 22 * sf;
asymmfilter1.sigma0    = 0.2 * sf;
asymmfilter1.alpha     = 0.1 *sf;

%% Filters responses
% Tresholds values
% DRIVE -> preprocessthresh = 0.5, thresh = 37
% STARE -> preprocessthresh = 0.5, thresh = 40
% CHASE_DB1 -> preprocessthresh = 0.1, thresh = 38
D1 = BCOSFIRE1(image1, symmfilter1);

symmfilter2 = struct();
symmfilter2.sigma     = 1.8* sf;
symmfilter2.len       = 8 * sf;
symmfilter2.sigma0    = 0.2* sf;
symmfilter2.alpha     = 0.1 * sf;

%% Asymmetric filter params
asymmfilter2 = struct();
asymmfilter2.sigma     = 0.6 * sf;
asymmfilter2.len       = 22 * sf;
asymmfilter2.sigma0    = 2 * sf;
asymmfilter2.alpha     = 0.1 *sf;

D2 = BCOSFIRE1(image1, symmfilter2);

symmfilter3 = struct();
symmfilter3.sigma     = 1.9* sf;
symmfilter3.len       = 8 * sf;
symmfilter3.sigma0    = 0.2* sf;
symmfilter3.alpha     = 0.1 * sf;

%% Asymmetric filter params
asymmfilter3 = struct();
asymmfilter3.sigma     = 0.8* sf;
asymmfilter3.len       = 22 * sf;
asymmfilter3.sigma0    = 2* sf;
asymmfilter3.alpha     = 0.1 *sf;

D3 = BCOSFIRE1(image1, symmfilter3);


symmfilter4 = struct();
symmfilter4.sigma     = 2.0 * sf;
symmfilter4.len       = 8 * sf;
symmfilter4.sigma0    = 0.2* sf;
symmfilter4.alpha     = 0.1 * sf;

%% Asymmetric filter params
asymmfilter4 = struct();
asymmfilter4.sigma     = 1* sf;
asymmfilter4.len       = 22 * sf;
asymmfilter4.sigma0    = 0.2 * sf;
asymmfilter4.alpha     = 0.1 *sf;

D4 = BCOSFIRE1(image1, symmfilter4);


symmfilter5 = struct();
symmfilter5.sigma     = 2.1 * sf;
symmfilter5.len       = 8 * sf;
symmfilter5.sigma0    =0.2* sf;
symmfilter5.alpha     = 0.1 * sf;

%% Asymmetric filter params
asymmfilter5 = struct();
asymmfilter5.sigma     = 1.2* sf;
asymmfilter5.len       = 22 * sf;
asymmfilter5.sigma0    = 2 * sf;
asymmfilter5.alpha     = 0.1 *sf;
D5 = BCOSFIRE1(image1, symmfilter5)
DP=zeros(Ay,Ax,3);
DP(:,:,1) = D1(:,:);
DP(:,:,2) = D2(:,:);
DP(:,:,3) = D3(:,:);
DP(:,:,2) = D4(:,:);
DP(:,:,3) = D4(:,:);
[D,sc] = max(DP,[],3);


% [output.respimage, oriensmap] = BCOSFIRE(image1, symmfilter, asymmfilter);
% output.segmented = (output.respimage > 5);
% imshow(output.segmented);
% BW2 =bwareaopen(output.segmented,130);%Removal of segments shorter than a maximum length
% figure('Name','Thresholded Image after removal of segments less than a length','NumberTitle','off');
imshow(D);
figure();imshow(uint8(D));