function detectionList = maximaPoints1(inputImage,output,params,show)



pixel_threshold =0.056;
% Detect local maxima points
maxoutput = zeros(size(inputImage));
pointlocation = [];

for n = 1:length(output)
    if any(output{n}(:) > pixel_threshold)
        disp(n);
        [row col] = find(imregionalmax(output{n} > pixel_threshold));
        pointlocation = [pointlocation; [row col]];
        maxoutput = max(maxoutput,double(output{n}));
    end
end

d = pdist(pointlocation);
z = linkage(d,'complete');
c = cluster(z,'cutoff',params.detection.mindistance,'Criterion','distance');
m = max(c(:));

detectionList = zeros(m,2);
for i = 1:m
    f = find(c == i);
    index = sub2ind(size(inputImage),pointlocation(f,1),pointlocation(f,2));
    [mx ind] = max(maxoutput(index));
    detectionList(i,:) = pointlocation(f(ind),:);
end

if show == 1
    figure;imshow(inputImage);colormap(gray);axis equal;axis off;hold on;
    plot(detectionList(:,2),detectionList(:,1),'r.','markersize',6);
%     for d = 1:length(detectionList)
%         text(detectionList(d,2),detectionList(d,1),num2str(d),'FontSize',8,'Color','red')
%     end
end
