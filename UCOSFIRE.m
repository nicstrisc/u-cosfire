function resp = UCOSFIRE(image, symmfilter3)
% 
%% Model configuration
% Prototype pattern

img = imread('prototype.png');
img = img/255;
prototype = double(img(:,:,1));
x=10; y =10;
%figure('Name','protype Image ','NumberTitle','off'); imshow(prototype);
 
% % Prototype pattern
symmfilter = cell(1);
symm_params = SystemConfig1;
symm_params.inputfilter.DoG.sigmalist = symmfilter3.sigma;
 symm_params.COSFIRE.rholist = 0:2:symmfilter3.len;
% symm_params.COSFIRE.rholist = [0,2,4,6,8];
symm_params.COSFIRE.sigma0 = symmfilter3.sigma0 / 6;
symm_params.COSFIRE.alpha = symmfilter3.alpha / 6;
% Orientations
numoriens = 36;
symm_params.invariance.rotation.psilist = 0:pi/numoriens:pi-pi/numoriens;
% Configuration
symmfilter{1} = configureCOSFIRE(prototype, round([y x]), symm_params);
showCOSFIREstructure(symmfilter{1});


rot1 = applyCOSFIRE(image, symmfilter);
rot1 = max(rot1{1},[],3);
resp = rot1;
