warning off;
close all;
clear all;
Im = imread('3.jpg');
% Im=imresize(Im,0.5);
A=Im;
figure('Name','Input image','NumberTitle','off');
imshow(A);
%[xc,yc] = ginput(1);% detecting Optic disc(OD) to find image center
xc=348;yc=294;
% xc=floor(xc);
% yc=floor(yc);
A=A(:,:,2);
figure('Name','green channel image ','NumberTitle','off');
imshow(A);
r=269%radius set to limit the area around the OD
[Ay,Ax]=size(A);
E=zeros(Ay,Ax);
image1 = double(A);
image1 = image1 ./ 255;
% vessel segmentation using multiscale B-COSFIRE filter.Here we used three
% scales
%% Symmetric filter params
symmfilter1 = struct();
symmfilter1.sigma     = 1.4;
symmfilter1.len       = 8 ;
symmfilter1.sigma0    = 0.8;
symmfilter1.alpha     = 0.5;


%% Asymmetric filter params
asymmfilter1 = struct();
asymmfilter1.sigma     = 0.6 ;
asymmfilter1.len       = 22 ;
asymmfilter1.sigma0    = 0.8 ;
asymmfilter1.alpha     = 0.5;
[D1,Agl1] = BCOSFIRE(image1, symmfilter1, asymmfilter1);

symmfilter2 = struct();
symmfilter2.sigma     = 2.2;
symmfilter2.len       = 8 ;
symmfilter2.sigma0    = 0.8;
symmfilter2.alpha     = 0.5;
%% Asymmetric filter params
asymmfilter2 = struct();
asymmfilter2.sigma     = 1.4;
asymmfilter2.len       = 22 ;
asymmfilter2.sigma0    = 0.8 ;
asymmfilter2.alpha     = 0.5 ;

[D2,Agl2] = BCOSFIRE(image1, symmfilter2, asymmfilter2);

symmfilter3 = struct();
symmfilter3.sigma     = 3.4;
symmfilter3.len       = 8 ;
symmfilter3.sigma0    = 0.8;
symmfilter3.alpha     = 0.5;

%% Asymmetric filter params
asymmfilter3 = struct();
asymmfilter3.sigma     = 2.4;
asymmfilter3.len       = 22 ;
asymmfilter3.sigma0    = 0.8;
asymmfilter3.alpha     = 0.5 ;

[D3,Agl3] = BCOSFIRE(image1, symmfilter3, asymmfilter3);



DP=zeros(Ay,Ax,3);
DP(:,:,1) = D1(:,:);
DP(:,:,2) = D2(:,:);
DP(:,:,3) = D3(:,:);

[D,sc] = max(DP,[],3); %taking maximum out of three scales.

for i=1:Ay
    for j=1:Ax
        E(i,j)=double(D(i,j));
    end
end

for i=1:Ax
    for j=1:Ay
        if (i>0)&&(i<=Ax)&&(j>0)&&(j<=Ay)
            if sqrt(((i-xc)^2)+((j-yc)^2))>=r
                E(j,i)=0;%Making image confined only to within radius r
            end
        end
    end
end
% for i=1:Ax
%     for j=1:Ay
%         if (i>0)&&(i<=Ax)&&(j>0)&&(j<=Ay)
%          if ((((i-xc)^2)/(78^2))+(((j-yc)^2)/(80^2)))<=1 %set the OD
%          dimension here for removing the OD.
%           E(j,i)=0;%Removal of OD from image
%          end
%         end
%     end
% end
for i=1:Ax
    for j=1:15
        E(j,i)=0;
    end
end
for i=1:Ax
    for j=Ay-14:Ay
        E(j,i)=0;
    end
end



mi=min(min(E));
ma=max(max(E));
E=(E-mi)*255/(ma-mi);%COSFIRE-magnitude-response
figure('Name',' Multiscale COSFIRE response','NumberTitle','off');
imshow(uint8(E));
I=uint8(E);

%% binarization
I=uint8(E);
filter = fspecial('gaussian', [3, 3], 1);
blurred = imfilter(I, filter, 'same');

level = graythresh(blurred); % Graythresh() utilizes Otsu's method
BW = im2bw(blurred, level);
% Generate list of pixels from region of interest
[height, width] = size(I);
pixel_list = [];

for x = linspace(1, height, height)
    for y = linspace(1, width, width)
        if BW(x, y) == 1
            pixel_list = [pixel_list, I(x,y)];
        end
    end
end

% Calculate threshold level using Otsu's method for region of interest
% found in previous step
level = graythresh(pixel_list);
level = level - 0.25 %Sensitivity parameter, change the value 0.25 for fine tuning


% Perform image thresholding using OTSU level for region of interest to
% select the blood vessels
binary_image_ROI = im2bw(blurred, level);
figure('Name','binary image after second thresholding','NumberTitle','off');
imshow(binary_image_ROI);
fontSize=10;
BW2=binary_image_ROI;
BW2=bwareaopen(BW2,5);%Removal of segments shorter than a maximum length
BW3=bwmorph(BW2,'skel',inf);%skeletonizing
figure('Name','Thinned image','NumberTitle','off');
imshow(BW3)
BW4 = bwmorph(BW3,'spur',5);%removal of spurs
%BW4 =bwareaopen(BW3,1);
figure('Name','Thinned vessel map after removal of spur','NumberTitle','off');
imshow(BW4)
%% U-COSFIRE filter response

Image =BW4;
symmfilter3 = struct();
symmfilter3.sigma     =0.52;
symmfilter3.len       = 8.0;
symmfilter3.sigma0    =0.8;
symmfilter3.alpha     =0.5;
D4 = UCOSFIRE(Image, symmfilter3);

original = D4;
imshow(original,[]);
test =bwareaopen(original,6);%Removal of segments shorter than a maximum length
symm_params.detection.mindistance = 20;
output.respimage = {original};
%For finding local maxima
detectionList = maximaPoints1(A,output.respimage,symm_params,1);
% detectionList = maximaPoints_adaptive(image1,output.respimage,symm_params,1);
disp(length(detectionList))
%   D5=(original>0.0005);% Threshold value set after experimentation
%  NZ = nnz(D5)
%  SD=nnz(D5)/prod(size(D5))
%  e = entropy(D5)
%Connected component labelling
original=test;
[rows, columns, numberOfColorBands] = size(original);

[labeledImage, numBlobs] = bwlabel(test);

measurements = regionprops(labeledImage, 'Area');
allAreas = [measurements.Area];

% Let's assign each blob a different color to visually show the user the distinct blobs.
coloredLabels = label2rgb (labeledImage, 'hsv', 'k', 'shuffle'); % pseudo random color labels
% coloredLabels is an RGB image.  We could have applied a colormap instead (but only with R2014b and later
filter = fspecial('gaussian', [3, 3], 1);
labeledImage = imfilter(coloredLabels, filter, 'same');
imshow(2.9*labeledImage);
axis image; % Make sure image is not artificially stretched because of screen's aspect ratio.
caption = sprintf('Pseudo colored labels, from label2rgb().\n%d Blobs are numbered from top to bottom, then from left to right...', numBlobs);
title(caption, 'FontSize', fontSize);

Addi=uint8(((double(labeledImage)))+((double(A))));
figure('Name','Retinex output image with marking of skeleton segments as tortuous(red) or normal(green)','NumberTitle','off');
imshow(Addi);
% result=[numBlobs;length(detectionList);NZ;SD;e]
